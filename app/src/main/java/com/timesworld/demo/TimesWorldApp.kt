package com.timesworld.demo

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-02
 */
class TimesWorldApp : Application() {

    override fun onCreate() {
        super.onCreate()

        val config = ImagePipelineConfig.newBuilder(this)
            .setDownsampleEnabled(true)
            .build()

        Fresco.initialize(this, config)
    }
}