package com.timesworld.demo.dataHolders

import com.timesworld.demo.utils.extractArray
import com.timesworld.demo.utils.extractObject
import com.timesworld.demo.utils.getBoolValue
import com.timesworld.demo.utils.getStringValue
import org.json.JSONObject

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-02
 */
class UserDetails(json: JSONObject) {
    val shortBio = json.extractObject("UserShortbio").getStringValue("Shortbio", "NIL")

    val location = json.extractObject("UserDetails").getStringValue("Location", "NIL")
    val workDetails = arrayListOf<String>()
    val educations = arrayListOf<String>()
    val skills = arrayListOf<String>()
    val achievements = arrayListOf<String>()
    val certificates = arrayListOf<String>()

    init {
        val worksJSON = json.extractArray("WorkExperience")
        for (i in 0 until worksJSON.length()) {
            val workJSON = worksJSON.getJSONObject(i)
            val designation = workJSON.getStringValue("Designation")
            val companyName = workJSON.getStringValue("CompanyName")
            val sector = workJSON.getStringValue("GvSector")
            val isCurrentCompany = workJSON.getBoolValue("IsCurrentCompany")
            val text = " ${if (isCurrentCompany) "Working" else "Worked"} as $designation at ${if (sector.isNotEmpty()) "$sector, " else ""} $companyName"
            workDetails.add(text)
        }

        val educationJSON = json.extractArray("UserEducationDetails")
        for (i in 0 until educationJSON.length()) {
            val json = educationJSON.getJSONObject(i)
            val course = json.getStringValue("Course")
            val institution = json.getStringValue("Institution")
            educations.add("$course from $institution")
        }

        val skillsJSON = json.extractArray("UserSkillDetails")
        for (i in 0 until skillsJSON.length()) {
            skills.add(skillsJSON.get(i) as? String ?: "Default Skill")
        }

        val achievementsJSON = json.extractArray("UserAchivements")
        for (i in 0 until achievementsJSON.length()) {
            achievements.add(achievementsJSON.getJSONObject(i).getStringValue("Achivement", "-"))
        }

        val certificatesJSON = json.extractArray("UserCertificates")
        for (i in 0 until certificatesJSON.length()) {
            certificates.add(certificatesJSON.getJSONObject(i).getStringValue("FileName", "-"))
        }
    }
}