package com.timesworld.demo.dataHolders

import com.timesworld.demo.utils.getStringValue
import org.json.JSONObject

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-02
 */
class UserDisplayData(val image: String, displayJSON: JSONObject, jobJSON: JSONObject) {
    val name = "${displayJSON.getStringValue("FirstName", "-")} ${displayJSON.getStringValue("LastName", "-")}"
    val tagLine = displayJSON.getStringValue("OverView", "")

    val designation = jobJSON.getStringValue("JobTitle", "-")
    val company = jobJSON.getStringValue("CompanyName", "-")
    val department = jobJSON.getStringValue("Department", "")

    val followers = displayJSON.getStringValue("", "0")
    val following = displayJSON.getStringValue("", "0")
}