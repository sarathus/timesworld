package com.timesworld.demo.dataHolders

import com.timesworld.demo.utils.Config
import com.timesworld.demo.utils.convertDate
import com.timesworld.demo.utils.getBoolValue
import com.timesworld.demo.utils.getStringValue
import org.json.JSONObject

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-02
 */

class UserPosts(val userImage: String,val userName: String, json: JSONObject) {
    val text = json.getStringValue("PostText")
    val date = json.getStringValue("MCreatedTime").convertDate()
    val startDate = json.getStringValue("StartDate").convertDate()
    val endDate = json.getStringValue("EndDate").convertDate()
    val attendeesCount = json.getStringValue("AttendeesCount")

    val imageUrl: String =
        if (json.getBoolValue("containsMedia") && json.getStringValue("MediaType") == "IMAGE") {
            "${Config.baseURL}${json.getStringValue("MediaUrl")}"
        } else {
            ""
        }

}