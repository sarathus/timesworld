package com.timesworld.demo.networking

import android.content.Context
import com.timesworld.demo.utils.getStringValue
import org.json.JSONObject

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-03
 */
class APIManager(val context: Context) {

    private fun checkForError(response: JSONObject, successBlock: (error: String?) -> Unit) {
        if (response.has("IsSuccess") && !response.getBoolean("IsSuccess")) {
            val error = response.getStringValue("Message", "Error while fetching data from server")
            successBlock(error)
            return
        }
        successBlock(null)
    }

    fun GET(
        url: String,
        params: HashMap<String, String>? = null,
        errorHandler: (String)->Unit, handler: ((response: JSONObject) -> Unit)? = null
    ) {

        VolleySingleton.getInstance(context)
            .GET(url, params ?: HashMap(), errorHandler){ response ->
                checkForError(response) { error ->
                    if (error != null) {
                        errorHandler.invoke(error)
                        return@checkForError
                    }
                    handler?.invoke(response)
                }
            }

    }


}