package com.timesworld.demo.networking

import com.timesworld.demo.utils.Config

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-03
 */
class Urls {

    companion object {
        const val getDetails = "${Config.baseURL}api/userprofile/getuserdetails"
    }
}