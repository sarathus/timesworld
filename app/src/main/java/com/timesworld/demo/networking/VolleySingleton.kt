package com.timesworld.demo.networking

import android.content.Context
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.TimeoutError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.timesworld.demo.utils.Config
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-03
 */
class VolleySingleton (private val context: Context) {

    companion object{
        @Volatile
        private var INSTANCE: VolleySingleton? = null

        fun getInstance(context: Context) = INSTANCE
            ?: synchronized(this) {
            INSTANCE
                ?: VolleySingleton(context).also {
                INSTANCE = it
            }
        }
    }

    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context.applicationContext)
    }

    private fun <T>  addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }

    fun GET(url: String, params: HashMap<String, String> = HashMap(),
             errorHandler: ((error: String)->Unit)? = null, responseHandler: ((response: JSONObject)->Unit)? = null ) {

        Config.logMessage("Posting with params: ${params}\n to : $url\n")
        val request = object: StringRequest(Request.Method.GET, url, { response ->
            Config.logMessage("Finished $url with response: \n $response")
            try{
                val responseJSON = JSONObject(response)
                responseHandler?.invoke(responseJSON)
            } catch(e: JSONException) {
                try {
                    val responseJSON = JSONObject()
                    val responseArray = JSONArray(response)
                    responseJSON.put("data", responseArray)
                    responseHandler?.invoke(responseJSON)
                }catch (e: JSONException) {
                    errorHandler?.invoke("Network error occurred. Unknown Format")
                }
            }
        }, {volleyError ->
            var message: String? = null
            if (volleyError is NoConnectionError) {
                message = "Unable to connect. Please check your internet connection and try again."
            } else if (volleyError is TimeoutError) {
                message = "Connection timed out. Please try again!"
            }
            Config.logMessage("Finished $url with error: \n $message")
            errorHandler?.invoke(message ?: "Network error occurred! please try again later.")
        })  {
            override fun getParams(): MutableMap<String, String> {
                return params
            }

            override fun getHeaders(): MutableMap<String, String> {
                return  HashMap()
            }
        }

        addToRequestQueue(request)
    }
}