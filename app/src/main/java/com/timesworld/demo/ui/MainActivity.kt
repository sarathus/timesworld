package com.timesworld.demo.ui

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.timesworld.demo.*
import com.timesworld.demo.networking.APIManager
import com.timesworld.demo.networking.Urls
import com.timesworld.demo.utils.BaseActivity
import com.timesworld.demo.utils.extractArray
import com.timesworld.demo.utils.extractObject
import com.timesworld.demo.utils.getStringValue
import com.timesworld.demo.viewmodels.MainActivityViewModel

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : BaseActivity() {

    private lateinit var viewModel: MainActivityViewModel
    private lateinit var adapter: RVAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)

        adapter = RVAdapter(this)

        infoRF.apply {
            adapter = this@MainActivity.adapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        fab.setOnClickListener { view ->

        }

        swipeToRefreshLayout.setOnRefreshListener {
            fetchData()
        }

        backButton?.setOnClickListener {
            onBackPressed()
        }

        viewModel.dataList.observeForever {
            adapter.setDataList(it)
        }

        fetchData()
    }

    private fun fetchData() {
        if (!swipeToRefreshLayout.isRefreshing) {
            swipeToRefreshLayout.isRefreshing = true
        }

        APIManager(this)
            .GET(Urls.getDetails, errorHandler = { error ->
                swipeToRefreshLayout.isRefreshing = false
                Toast.makeText(this, error, Toast.LENGTH_LONG).show()
            }) { response ->
                swipeToRefreshLayout.isRefreshing = false

                viewModel.setupData(response)

                adapter.notifyDataSetChanged()
            }
    }

}
