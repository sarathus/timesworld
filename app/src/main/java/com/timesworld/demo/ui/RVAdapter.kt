package com.timesworld.demo.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.timesworld.demo.R
import com.timesworld.demo.dataHolders.UserDetails
import com.timesworld.demo.dataHolders.UserDisplayData
import com.timesworld.demo.dataHolders.UserPosts
import com.timesworld.demo.utils.BaseVH

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-02
 */
class RVAdapter(private val context: Context) : RecyclerView.Adapter<BaseVH>() {

    private val USER_PROFILE_VIEW_TYPE = 0
    private val USER_DETAILS_VIEW_TYPE = 1
    private val USER_POST_VIEW_TYPE = 2

    private var dataList = listOf<Any>()

    private val layoutInflater = LayoutInflater.from(context)

    fun setDataList(dataList: List<Any>) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseVH {
        return when (viewType) {
            USER_PROFILE_VIEW_TYPE -> ProfileDisplayVH(
                layoutInflater.inflate(
                    R.layout.profile_diplay_vh,
                    parent,
                    false
                )
            )
            USER_DETAILS_VIEW_TYPE -> UserDetailsVH(
                layoutInflater.inflate(
                    R.layout.profile_details_vh,
                    parent,
                    false
                )
            )
            else -> UserPostVH(layoutInflater.inflate(R.layout.user_posts_vh, parent, false))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (dataList[position]) {
            is UserDisplayData -> USER_PROFILE_VIEW_TYPE
            is UserDetails -> USER_DETAILS_VIEW_TYPE
            else -> USER_POST_VIEW_TYPE
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: BaseVH, position: Int) {
        holder.setData(position)
    }

    inner class ProfileDisplayVH(v: View) : BaseVH(v) {
        private val nameTV = v.findViewById<TextView>(R.id.nameTV)
        private val infoTV = v.findViewById<TextView>(R.id.infoTV)
        private val tagLineTV = v.findViewById<TextView>(R.id.tagLineTV)
        private val followersTV = v.findViewById<TextView>(R.id.followersTV)
        private val followingTV = v.findViewById<TextView>(R.id.followingTV)
        private val userIV = v.findViewById<SimpleDraweeView>(R.id.userIV)

        override fun setData(position: Int) {
            val item = dataList[position] as? UserDisplayData
                ?: return

            nameTV.text = item.name
            tagLineTV.text = item.tagLine
            infoTV.text =
                "${item.designation}\n${item.company}${if (item.department.isNotEmpty()) ", ${item.department}" else ""}"
        }
    }

    inner class UserDetailsVH(v: View) : BaseVH(v) {
        private val shortBioTV = v.findViewById<TextView>(R.id.shortBioTV)
        private val workHolderLL = v.findViewById<LinearLayout>(R.id.workHolderLL)
        private val educationHolderLL = v.findViewById<LinearLayout>(R.id.educationHolderLL)
        private val skillsHolderLL = v.findViewById<LinearLayout>(R.id.skillsHolderLL)
        private val achievementsHolderLL = v.findViewById<LinearLayout>(R.id.achievementsHolderLL)
        private val certificatesHolderLL = v.findViewById<LinearLayout>(R.id.certificatesHolderLL)
        private val locationHolderLL = v.findViewById<LinearLayout>(R.id.locationHolderLL)

        override fun setData(position: Int) {
            val item = dataList[position] as? UserDetails
                ?: return

            shortBioTV.text = item.shortBio

            workHolderLL.removeAllViews()
            educationHolderLL.removeAllViews()
            skillsHolderLL.removeAllViews()
            achievementsHolderLL.removeAllViews()
            certificatesHolderLL.removeAllViews()
            locationHolderLL.removeAllViews()

            if (item.workDetails.isNotEmpty()) {
                for (work in item.workDetails) {
                    addEntries(workHolderLL,
                        R.drawable.ic_work, work)
                }
            } else {
                addEntries(workHolderLL,
                    R.drawable.ic_work, "")
            }

            if (item.educations.isNotEmpty()) {
                for (education in item.educations) {
                    addEntries(educationHolderLL,
                        R.drawable.ic_education, education)
                }
            } else {
                addEntries(educationHolderLL,
                    R.drawable.ic_education, "")
            }

            var userSkills = ""
            for (skill in item.skills) {
                userSkills = "${if (userSkills.isNotEmpty()) "$userSkills," else userSkills} $skill"
            }
            addEntries(skillsHolderLL,
                R.drawable.ic_skills, userSkills)

            var userAchievements = ""
            for (achievement in item.achievements) {
                userAchievements = "${if (userAchievements.isNotEmpty()) "$userAchievements," else userAchievements} $achievement"
            }
            addEntries(achievementsHolderLL,
                R.drawable.ic_achievements, userAchievements)

            if (item.certificates.isNotEmpty()) {
                for (achievement in item.achievements) {
                    addEntries(certificatesHolderLL,
                        R.drawable.ic_certificates, achievement)
                }
            } else {
                addEntries(certificatesHolderLL,
                    R.drawable.ic_certificates, "")
            }

            addEntries(locationHolderLL,
                R.drawable.ic_location, item.location)

        }
    }

    inner class UserPostVH(v: View) : BaseVH(v) {

        private val userIV = v.findViewById<SimpleDraweeView>(R.id.userIV)
        private val nameTV = v.findViewById<TextView>(R.id.nameTV)
        private val descTV = v.findViewById<TextView>(R.id.descTV)
        private val timingsTV = v.findViewById<TextView>(R.id.timingsTV)
        private val createdTV = v.findViewById<TextView>(R.id.createdTV)
        private val attendeesTV = v.findViewById<TextView>(R.id.attendeesTV)
        private val mediaIV = v.findViewById<SimpleDraweeView>(R.id.mediaIV)

        override fun setData(position: Int) {
            val item = dataList[position] as? UserPosts
                ?: return

            descTV.text = item.text
            timingsTV.text = "${item.startDate} to ${item.endDate}"
            attendeesTV.text = "Attendees: ${item.attendeesCount}"
            createdTV.text = item.date

            if (item.imageUrl.isNotEmpty()) {
                mediaIV.setImageURI(item.imageUrl)
            } else {
                mediaIV.visibility = View.GONE
            }

            userIV.setImageURI(item.userImage)
            nameTV.text = item.userName
        }
    }

    private fun addEntries(parent: LinearLayout, icon: Int, text: String) {
        val entryView = layoutInflater.inflate(R.layout.layout_entry_item, parent, false)
        val iconIV = entryView.findViewById<AppCompatImageView>(R.id.iconIV)
        val nameTV = entryView.findViewById<TextView>(R.id.nameTV)

        iconIV.setImageResource(icon)
        nameTV.text = text

        parent.addView(entryView)
    }
}