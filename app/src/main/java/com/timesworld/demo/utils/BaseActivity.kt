package com.timesworld.demo.utils

import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-02
 */
open class BaseActivity: AppCompatActivity() {

}