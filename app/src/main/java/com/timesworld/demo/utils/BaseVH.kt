package com.timesworld.demo.utils

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-02
 */
open class BaseVH(v: View): RecyclerView.ViewHolder(v) {

    open fun setData(position: Int) {

    }
}