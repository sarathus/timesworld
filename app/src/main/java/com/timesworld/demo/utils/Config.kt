package com.timesworld.demo.utils

import android.util.Log

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-03
 */
class Config {

    companion object {

        const val baseURL = "https://engagedxbv3.datasight.biz/"

        const val isProductionMode: Boolean = false

        fun logMessage(message: String) {
            if (isProductionMode) {
                Log.i("sarath_u_s", message)
            }
        }
    }
}