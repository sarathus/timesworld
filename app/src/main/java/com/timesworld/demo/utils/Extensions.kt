package com.timesworld.demo.utils

import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-02
 */

fun JSONObject.getStringValue(key: String, default: String = ""): String =
    if (has(key)) {
        val value = getString(key)
        if (value.toLowerCase() == "null") {
            default
        } else {
            value
        }
    } else {
        default
    }

fun JSONObject.getBoolValue(key: String, default: Boolean = false): Boolean =
    if (has(key)) {
        val value = get(key)
        if(value is Boolean) {
            value
        } else {
            default
        }
    } else {
        default
    }

fun JSONObject.extractArray(key: String): JSONArray {
    if (has(key)) {
        get(key).let {
            if (it is JSONArray) {
                return it
            }
        }
    }
    return JSONArray()
}

fun JSONObject.extractObject(key: String): JSONObject {
    if (has(key)) {
        get(key).let {
            if (it is JSONObject) {
                return it
            }
        }
    }
    return JSONObject()
}

fun String.convertDate(): String {
    try {
        val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val outFormat = SimpleDateFormat("dd-MM-yyyy HH:mm a")
        val date = simpleDateFormat.parse(this) ?: return this

        return outFormat.format(date)
    } catch (e: Exception) {
        e.printStackTrace()
        return this
    }
}
