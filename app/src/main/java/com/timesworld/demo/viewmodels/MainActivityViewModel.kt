package com.timesworld.demo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.timesworld.demo.dataHolders.UserDetails
import com.timesworld.demo.dataHolders.UserDisplayData
import com.timesworld.demo.dataHolders.UserPosts
import com.timesworld.demo.utils.Config
import com.timesworld.demo.utils.extractArray
import com.timesworld.demo.utils.extractObject
import com.timesworld.demo.utils.getStringValue
import org.json.JSONObject

/**
 * Created by Sarath U S, sarath.us44@gmail.com on 2020-03-03
 */
class MainActivityViewModel: ViewModel() {

    var dataList = MutableLiveData<List<Any>>()

    init {
        dataList.value = listOf()
    }

    fun setupData(response: JSONObject) {

        val dataJSON = response.extractObject("ResponseData")
        val postsJSON = dataJSON.extractArray("UserPosts")

        var userImage = dataJSON.extractObject("DisplayData").getStringValue("ProfileImage")
        if (!userImage.toLowerCase().contains("http")) {
            userImage = "${Config.baseURL}$userImage"
        }
        val userName = dataJSON.extractObject("DisplayData").getStringValue("Name")

        val userDisplay = UserDisplayData(
            userImage, dataJSON.extractObject("UserDetails"),
            dataJSON.extractObject("CurrentCompany")
        )

        val dataList = arrayListOf<Any>()

        dataList.add(userDisplay)
        dataList.add(UserDetails(dataJSON))

        for (i in 0 until postsJSON.length()) {
            dataList.add(
                UserPosts(
                    userImage,
                    userName,
                    postsJSON.getJSONObject(i)
                )
            )
        }

        this.dataList.value = dataList

    }
}